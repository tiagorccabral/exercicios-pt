# Escreva um algoritmo que dada uma string, chama a função palindrome?. 
# Está função deve retornar um valor booleano que indica se a palavra ou frase digitada é um palíndromo. 
# A string não contém caracteres diferentes de espaços ou letras.

# Exemplos:
# def palindrome?(string)
#   seu codigo aqui
# end

# palindrome?("A man a plan a canal Panama") #=> true
# palindrome?("Roma e amor")
# => true

# palindrome?("Abracadabra")
# => false (ou nil)