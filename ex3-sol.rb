# Escreva um algoritmo que dada uma string, chama a função palindrome?. 
# Está função deve retornar um valor booleano que indica se a palavra ou frase digitada é um palíndromo. 
# A string não contém caracteres diferentes de espaços ou letras.

# Exemplos:
# def palindrome?(string)
#   seu codigo aqui
# end

# palindrome?("A man a plan a canal Panama") #=> true
# palindrome?("Roma e amor")
# => true

# palindrome?("Abracadabra")
# => false (ou nil)

def palindrome? (str)
    normalized_str = str.chomp.gsub(/\s/, "").downcase
    inverted_str = str.chomp.gsub(/\s/, "").reverse.downcase
    if (normalized_str == inverted_str)
        puts "Eh palindromo"
    else
        puts "Nao eh palindromo"
    end
end


palindrome? ("Araba")